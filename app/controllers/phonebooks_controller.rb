class PhonebooksController < ApplicationController
  before_action :set_phonebook, only: %i[ show edit update destroy ]

  # GET /phonebooks or /phonebooks.json
  def index
    @phonebooks = Phonebook.select("first_name", "last_name", "id").reverse_order
    @contact_count = Phonebook.count

    respond_to do |format|
      format.turbo_stream
      format.html
    end
  end

  # GET /phonebooks/1 or /phonebooks/1.json
  def show
    respond_to do |format|
      format.json {render json: @phonebook, status: :ok}
      format.html
    end
  end

  # GET /phonebooks/new
  def new
    @phonebook = Phonebook.new
    respond_to do |format|
      format.turbo_stream
      format.html
    end
  end

  # GET /phonebooks/1/edit
  def edit
    respond_to do |format|
      format.turbo_stream
      format.html
    end
  end

  # POST /phonebooks or /phonebooks.json
  def create
    @phonebook = Phonebook.new(phonebook_params)

    respond_to do |format|
      if @phonebook.save
        @phonebooks = Phonebook.select("first_name", "last_name", "id").reverse_order
        @contact_count = Phonebook.count
        format.turbo_stream
        format.html { redirect_to phonebook_url(@phonebook), notice: "Phonebook was successfully created." }
        format.json { render :show, status: :created, location: @phonebook }
      else
        format.turbo_stream {render turbo_stream: turbo_stream.update('alert-box', @phonebook.errors.full_messages)}
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @phonebook.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /phonebooks/1 or /phonebooks/1.json
  def update
    respond_to do |format|
      if @phonebook.update(phonebook_params)
        @phonebooks = Phonebook.select("first_name", "last_name", "id").reverse_order
        format.turbo_stream
        format.html { redirect_to phonebook_url(@phonebook), notice: "Phonebook was successfully updated." }
        format.json { render :show, status: :ok, location: @phonebook }
      else
        format.turbo_stream {render turbo_stream: turbo_stream.update('alert-box', @phonebook.errors.full_messages)}
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @phonebook.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phonebooks/1 or /phonebooks/1.json
  def destroy
    @phonebook.destroy
    @contact_count = Phonebook.count

    respond_to do |format|
      format.turbo_stream
      format.html { redirect_to phonebooks_url, notice: "Phonebook was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phonebook
      @phonebook = Phonebook.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def phonebook_params
      params.require(:phonebook).permit(:first_name, :last_name, :email_address, :phone)
    end
end
