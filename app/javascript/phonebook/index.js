const methods = {
    btnCreateClick() {
        const modal = new bootstrap.Modal(document.getElementById('modal-form-create'), {})
        modal.show()
    },
    btnEditClick() {
        const id = event.target.attributes['data-id'].value
        const form = document.getElementById('modal-form-edit').childNodes[1].childNodes[1].childNodes[3]
        const baseUrl = `${window.location}phonebooks`
        form.action = `${baseUrl}/${id}`
        form.method = 'patch'
        const modal = new bootstrap.Modal(document.getElementById('modal-form-edit'), {})
        $.ajax({
            type: 'GET',
            url: `${baseUrl}/${id}`,
            contentType: "application/json",
            dataType: "json",
            success: (data) => {
                methods.injectDataToForm(form, data)
            }
        })
        modal.show()
    },
    btnShowClick() {
        const id = event.target.attributes['data-id'].value
        const modal = new bootstrap.Modal(document.getElementById('modal-detail'), {})
        const baseUrl = `${window.location}phonebooks`
        $.ajax({
            type: 'GET',
            contentType: "application/json",
            dataType: "json",
            url: `${baseUrl}/${id}`,
            success: (data) => {
                methods.injectDataToModal(data)
                modal.show()
            }
        })
    },
    injectDataToForm(form, data) {
        form['phonebook_first_name'].value = data.first_name
        form['phonebook_last_name'].value = data.last_name
        form['phonebook_phone'].value = data.phone
        form['phonebook_email_address'].value = data.email_address
    },
    injectDataToModal(data) {
        const field = document.getElementById('detail-field')
        field.innerHTML = ''
        const name_label = document.createElement('dt')
        name_label.innerHTML = "Nama Lengkap"
        field.appendChild(name_label)
        const name_field = document.createElement('dd')
        name_field.innerHTML = `${data.first_name} ${data.last_name}`
        field.appendChild(name_field)
        const email_label = document.createElement('dt')
        email_label.innerHTML = "Email"
        field.appendChild(email_label)
        const email_field = document.createElement('dd')
        email_field.innerHTML = `${data.email_address}`
        field.appendChild(email_field)
        const phone_label = document.createElement('dt')
        phone_label.innerHTML = "Telepon"
        field.appendChild(phone_label)
        const phone_field = document.createElement('dd')
        phone_field.innerHTML = `${data.phone}`
        field.appendChild(phone_field)
    },
}

document.addEventListener('DOMContentLoaded', (event) => {
    const btnCreate = document.getElementsByClassName('btn-create')
    Array.from(btnCreate).forEach(btn => {
        btn.addEventListener('click', methods.btnCreateClick)
    })
    const btnEdits = document.getElementsByClassName('btn-edit')
    Array.from(btnEdits).forEach(btn => {
        btn.addEventListener('click', methods.btnEditClick)
    })
})