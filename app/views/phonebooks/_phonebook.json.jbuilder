json.extract! phonebook, :id, :first_name, :last_name, :email, :phone, :created_at, :updated_at
json.url phonebook_url(phonebook, format: :json)
