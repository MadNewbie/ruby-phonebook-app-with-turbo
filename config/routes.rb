Rails.application.routes.draw do
  resources :phonebooks
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "phonebooks#index"
end
