class CreatePhonebooks < ActiveRecord::Migration[7.0]
  def change
    create_table :phonebooks do |t|
      t.string :first_name
      t.string :last_name
      t.string :email_address
      t.string :phone

      t.timestamps
    end
  end
end
